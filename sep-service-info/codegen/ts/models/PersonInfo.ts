/* tslint:disable */
/* eslint-disable */
/**
 *SEP Info service
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.0.4
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
/**
 * 
 * @export
 * @interface PersonInfo
 */
export interface PersonInfo {
    /**
     * 
     * @type {string}
     * @memberof PersonInfo
     */
    name: string;
    /**
     * 
     * @type {number}
     * @memberof PersonInfo
     */
    takeOffId?: number;
    /**
     * 
     * @type {string}
     * @memberof PersonInfo
     */
    personStatistic?: string;
    /**
     * 
     * @type {string}
     * @memberof PersonInfo
     */
    years?: string;
    /**
     * queue | ready | taken
     * @type {string}
     * @memberof PersonInfo
     */
    status?: string;
}

export function PersonInfoFromJSON(json: any): PersonInfo {
    return PersonInfoFromJSONTyped(json, false);
}

export function PersonInfoFromJSONTyped(json: any, ignoreDiscriminator: boolean): PersonInfo {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'name': json['name'],
        'takeOffId': !exists(json, 'takeOffId') ? undefined : json['takeOffId'],
        'personStatistic': !exists(json, 'personStatistic') ? undefined : json['personStatistic'],
        'years': !exists(json, 'years') ? undefined : json['years'],
        'status': !exists(json, 'status') ? undefined : json['status'],
    };
}

export function PersonInfoToJSON(value?: PersonInfo | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'name': value.name,
        'takeOffId': value.takeOffId,
        'personStatistic': value.personStatistic,
        'years': value.years,
        'status': value.status,
    };
}


